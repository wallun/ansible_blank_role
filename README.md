# ansible_blank_role

An ansible role template with pre-commit and gitlab-ci configured.

## Requirements

1. pre-commit
1. gitlint
1. ansible-lint
1. markdownlint

### Installing requirements on Archlinux

On Archlinux, these requirements can be met by running
`pacman -S pre-commit ansible-lint markdownlint`.

`gitlint` can be found on the [AUR](https://aur.archlinux.org/gitlint)

### Installing requirements on Ubuntu 22.04

On Ubuntu 22.04, these requirements can be met by running:

- `apt install gitlint rubygems python3-pip`
- `gem install mdl`
- `pip3 install ansible-lint pre-commit`

## How to use this repository

1. Clone the repository by running
  `git clone https://gitlab.archlinux.org/wallun/ansible_blank_role`
1. Delete the `.git` folder and init yours with `git init`.
1. Add the *pre-commit* hooks by running
  `pre-commit install -t pre-commit` and `pre-commit install -t commit-msg`.
1. Create your role
1. Update `README.md`
1. Commit on another branch that `main`, as it is protected.

Your code will be committed only if pre-commit runs with success.

Edit either `.gitlint`, `.mdlintrc`, `yamllint` or `pre-commit-config.yaml` to
modify the rules.

## Configure gitlab-ci

If you're using Gitlab-CI, you might want to configure your repository with the
path of `gitlab-ci.yaml`.

In `https://YOURGITLAB.DOMAIN.TLD/GROUP_PATH/ROLE_NAME/-/settings/ci_cd`, set
`CI/CD configuration file` to `.ci/gitlab-ci.yaml`.
